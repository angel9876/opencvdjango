from django.db import models

class Color(models.Model):
    color = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
