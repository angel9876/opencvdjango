import cv2
import numpy as np
import os
import time
from django.shortcuts import render
from .models import Color

from django.http import JsonResponse

def procesar_imagen(request):
    img_file = "C:/Users/Angel Armijos/Documents/Z Proyect/venvs/OpenCvDjango/app_opencv/my_app/img/GG 1.jpg"
    color_list = []

    # Leer imagen
    imagen = cv2.imread(img_file)
    ancho = 600
    alto = 400

    if imagen is None:
        print("Error al cargar la imagen.")
        # Manejo de error o salida del programa
    else:
        # Redimensionar imagen
        imagen_redismensionada = cv2.resize(imagen, (ancho, alto))
        # Convertir a escala de grises
        imagen_gris = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)
        redimensionada = cv2.resize(imagen_gris, (ancho, alto))
        # Aplicar filtros y procesamiento de imagen
        imgBlur = cv2.GaussianBlur(redimensionada, (3, 3), 1)
        imgTheshold = cv2.adaptiveThreshold(imgBlur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 25,
                                            16)
        imgMedian = cv2.medianBlur(imgTheshold, 5)

        kernel = np.ones((3, 3), np.uint8)
        imgDilate = cv2.dilate(imgMedian, kernel, iterations=1)

        # Definir el area mediante un array
        pts_list = [
            [[55, 215], [-35, 295], [30, 295], [105, 215]],
            [[105, 215], [30, 295], [100, 295], [160, 215]],
            [[160, 215], [100, 295], [165, 295], [200, 215]],
            [[200, 215], [165, 295], [240, 295], [255, 215]],
            [[255, 215], [240, 295], [310, 295], [310, 215]],
            [[310, 215], [310, 295], [380, 295], [355, 215]],
            [[355, 215], [380, 295], [455, 295], [395, 215]],
            [[395, 215], [460, 295], [550, 295], [460, 215]],
            [[465, 215], [555, 295], [630, 295], [540, 215]]
        ]

        # Procesar imagen
        for i, pts in enumerate(pts_list, start=1):
            pts = np.array(pts, np.int32)
            pts = pts.reshape((-1, 1, 2))
            mask = np.zeros(imgDilate.shape[:2], np.uint8)
            cv2.fillPoly(mask, [pts], 255)
            img_area = cv2.bitwise_and(imgDilate, imgDilate, mask=mask)
            white_pixels = cv2.countNonZero(img_area)
            print(f"Píxeles blancos en área {i}: {white_pixels}")

            color = (0, 255, 0)  # Inicialmente verde
            if white_pixels > 700:
                color = (0, 0, 255)  # Cambiar a rojo si el umbral está por encima de 700

            cv2.polylines(imagen_redismensionada, [pts], isClosed=True, color=color, thickness=2)

            if color == (0, 255, 0):  # Verificar si el color es verde
                color_list.append(True)  # Agregar True a la lista
            else:
                color_list.append(False)

        print(color_list)  # Imprimir la lista de colores

        # Mostrar imagen
        cv2.imshow("imagen", imagen_redismensionada)
        cv2.waitKey(10000)

        # Borrar archivo de imagen
        os.remove(img_file)

        # Esperar 10 segundos
        time.sleep(10)

    # Guardar los colores en la base de datos
    for color in color_list:
        Color.objects.create(color=color)

    # Renderizar la plantilla con los datos actualizados
    context = {'color_list': color_list}
    return render(request, 'resultados.html', context)


def obtener_datos(request):
    color_list = [True if color.color else False for color in Color.objects.all()]
    return JsonResponse({'color_list': color_list})
